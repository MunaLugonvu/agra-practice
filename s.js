import fetch from 'node-fetch';
import { chromium } from '@playwright/test';

(async () => {
  const browser = await chromium.launch();
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto('https://startselect.com/gift-cards');

  await page.evaluate(() => {
    window.scrollTo(0, document.body.scrollHeight);
  });

  const categories = await page.evaluate(() => {
    const anchorElements = document.querySelectorAll('a.contents');

    return Array.from(anchorElements).map((anchorElement) => {
      const categoryElement = anchorElement.querySelector('.product-block__container');
      const titleElement = categoryElement.querySelector('.product-block__container__content__title');
      const infoElement = categoryElement.querySelector('.product-block__container__content__info');
      const imgElement = categoryElement.querySelector('.product-block__container__packshot img');
      const pricesElement = categoryElement.querySelector('.product-block__container__content__price');

      const imageURL = imgElement ? imgElement.src : null;

      return {
        title: titleElement ? titleElement.textContent.trim().split('\n')[0].trim() : '',
        info: infoElement ? infoElement.textContent.trim() : '',
        imageURL: imageURL,
        price_range: pricesElement ? pricesElement.textContent.trim() : '',
        category_link: anchorElement.href,
      };
    });
  });

  await browser.close();
  //console.log(categories);

  // Create blocks for each category
  const blocks = categories.map((category) => ({
    type: 'section',
    text: {
      type: 'mrkdwn',
      text: `*Title:* ${category.title}\n*Info:* ${category.info}\n*Price Range:* ${category.price_range}\n<Link|${category.category_link}>`,
    },
    accessory: {
      type: 'image',
      image_url: category.imageURL,
      alt_text: category.title,
    },
  }));

  // Define the Slack webhook URL
  const slackWebhookUrl = 'https://hooks.slack.com/services/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';

  // Prepare the payload to send to Slack
  const payload = {
    text: 'Here are the categories for UK:',
    blocks: blocks,
  };

  // Define the headers
  const headers = {
    'Content-Type': 'application/json',
  };

  // Send the payload to Slack
  await fetch(slackWebhookUrl, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(payload),
  })
    .then((res) => res.text())
    .then((responseBody) => console.log(responseBody))
    .catch((err) => console.error(err));
})();